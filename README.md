# Vagrantfile for Code-Aster #

This repository contains a Vagrantfile that will automatically install the dependencies for and then compile [Code-Aster](http://code-aster.org/) in an Ubuntu-based Virtual Machine.

## Instructions ##

To install Vagrant see the instructions for your platform at: (http://www.vagrantup.com/).

To install Virtualbox see the instructions for your platform at: (http://www.virtualbox.org/).

Download this repository, then navigate to the folder containing the Vagrantfile and run:

    vagrant up

and wait for the provisioning to finish. Then to connect to the Virtual Machine run:

    vagrant ssh

and you should see a Linux terminal prompt. The machine contains a working version of Code-Aster in `/opt/code-aster`.

To stop the virtual machine simply run (on the host):

    vagrant halt

To destroy (delete) the virtual machine run:

    vagrant destroy

## Updating to the latest version of Code-Aster ##

It is likely that this repository contains a link to an out-of-date version of Code-Aster. To change the version, update the URL in the SCRIPT section of the Vagrantfile to the preferred .tar.gz file on the Code-Aster website.

