# -*- mode: ruby -*-
# vi: set ft=ruby :

# Vagrantfile API/syntax version. Don't touch unless you know what you're doing!
VAGRANTFILE_API_VERSION = "2"

Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|
  # All Vagrant configuration is done here. The most common configuration
  # options are documented and commented below. For a complete reference,
  # please see the online documentation at vagrantup.com.

  # Every Vagrant virtual environment requires a box to build off of.
  config.vm.box = "ubuntu-trusty-server-cloudimg"
  config.vm.box_url = "https://cloud-images.ubuntu.com/vagrant/trusty/current/trusty-server-cloudimg-amd64-vagrant-disk1.box"

  # Disable automatic box update checking. If you disable this, then
  # boxes will only be checked for updates when the user runs
  # `vagrant box outdated`. This is not recommended.
  # config.vm.box_check_update = false

  # Share an additional folder to the guest VM. The first argument is
  # the path on the host to the actual folder. The second argument is
  # the path on the guest to mount the folder. And the optional third
  # argument is a set of non-required options.
  # config.vm.synced_folder "../data", "/vagrant_data"
  
  config.ssh.forward_x11 = true

  config.vm.provider "virtualbox" do |vb|
     # Don't boot with headless mode
     vb.gui = false
     # Seems to require a lot of memory to compile
     # can be reduced for general running.
     vb.memory = 8192
  end

  config.vm.provision "shell", inline: $script
end

$script = <<SCRIPT
#!/bin/bash
# Roughly based on instructions at
# From http://www.code-aster.org/wiki/doku.php?id=en:p02_install:compil_ubuntu
apt-get update

# Install dependencies
apt-get install -y gfortran build-essential python-dev python-numpy zlib1g-dev libatlas-base-dev tk8.4 grace bison flex python-qt4

# Pull and compile code-aster
cd /tmp
wget --quiet http://www.code-aster.org/FICHIERS/aster-full-src-11.6.0-1.noarch.tar.gz
tar -xvf aster-full-src-11.6.0-1.noarch.tar.gz
cd aster-full-src-11.6.0
python setup.py install --ignore_error --noprompt

# Cleanup
apt-get clean
rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
date > /etc/vagrant_provisioned_at
SCRIPT

